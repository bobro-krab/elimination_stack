#ifndef LF_STACK
#define LF_STACK

#include <thread>
#include <future>
#include <atomic>
#include <iostream>

#include "backoff.h"
#include "hazard.h"

#define STACK_IS_EMPTY 1
#define NOT_EXCHANGED 2

using namespace std;
template <class T>
class node {
    public:
    T data;
    node * next;

    node(T init):
        next(nullptr)
    {
        data = init;
    }

    ~node()
    {
    }
};


template <class T>
class lockFreeStack {
protected:

    hazard_pointer_list<node<T>> hazard_pointers;
    const int MIN_DELAY = 1;
    const int MAX_DELAY = 100;
    class Backoff backoff = Backoff(MIN_DELAY, MAX_DELAY);
    atomic<node<T> *> top;

public:
    lockFreeStack():
        hazard_pointers(),
        backoff(MIN_DELAY, MAX_DELAY),
        top(ATOMIC_VAR_INIT(nullptr))
    {
    }

    ~lockFreeStack() {
    }

    void clear()
    {
        try {
            while(true) {
                pop();
            }
        }
        catch (int i) {}
    }

    virtual T pop()
    {
        hazard_pointer<node<T>> &hp_old_top = hazard_pointers.acquire();
        node<T> *old_top = nullptr, *temp = nullptr;
        old_top = top.load();

        do {
            do {
                temp = old_top;
                hp_old_top.remember(old_top);
                old_top = top.load();
            } while(old_top != temp);
        } while(old_top && !top.compare_exchange_strong(old_top, old_top->next));
        hp_old_top.release();

        if (old_top) {
            T return_data = old_top->data;
            if (hazard_pointers.contains(old_top)) {
                hazard_pointers.reclaim_later(old_top);
            } else {
                free(old_top);
            }
            hazard_pointers.delete_reclaimed();
            return return_data;
        } else {
            throw(STACK_IS_EMPTY);
        }
    }

    virtual bool push(T data)
    {
        // hazard_pointer<node<T>> &hp_old_top = hazard_pointers.acquire();
        node<T> new_node(data);
        node<T> *to_push = (node<T> *)malloc(sizeof(new_node));
        *to_push = new_node;

        node<T> *old_top = nullptr, *temp = nullptr;
        do {
            do {
                temp = old_top;
                old_top = top.load();
            } while(old_top != temp);
            to_push->next = old_top;
        } while(old_top && !top.compare_exchange_strong(old_top, to_push));
        return true;
    }

    virtual string get_statistics()
    {
        return "Empty statistics";
    }
};

#endif
