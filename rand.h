#ifndef RAND
#define RAND
//функции возвращают случайные значения подчиненные экспоненциальному распределению
#include <random>

int random_int(int a) {
    thread_local unsigned int seed = time(NULL);
    return rand_r(&seed) % a;
}

#endif
