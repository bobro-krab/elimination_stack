#!/bin/bash

gnuplot << EOP

set terminal png size 800,600
set output "$2"
set y2tics 100
set y2label "asdf"

plot "$1" using 1:3 title "Время выполнения" with lines axis x1y1, "$1" using 1:4 title "Elimination count" with lines axis x1y2

EOP
