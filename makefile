COMPILER=g++
CFLAGS=-g -Wall -pedantic -std=c++11 -latomic -lpthread
SOURSES=main.cpp
OBJECTS=$(SOURSES:.cpp=.o)
EXECUTABLE=prog

all: $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS)
	$(COMPILER) $(CFLAGS) $(OBJECTS) -o $@

$(OBJECTS):$(SOURSES)
	$(COMPILER) $(CFLAGS) -c $(SOURSES)

clean:
	rm $(OBJECTS) $(EXECUTABLE)
	rm -r doxy

run: $(EXECUTABLE)
	./$(EXECUTABLE)

send: $(EXECUTABLE) task.job
	scp ./* cluster:~/current/
#	ssh cluster

docs:
	doxygen Doxyfile
