#ifndef BACKOFF_H
#define BACKOFF_H 

#include <thread>

#include "rand.h"

using namespace std;


class Backoff {
    int minDelay, maxDelay;
    int limit;

public:
    Backoff(int min, int max):
        minDelay(min),
        maxDelay(max),
        limit(minDelay){
    }

    void backoff() {
        int delay = random_int(limit);
        limit = min(maxDelay, 2 * limit);
        this_thread::sleep_for(chrono::microseconds(delay));
    }
};
#endif
