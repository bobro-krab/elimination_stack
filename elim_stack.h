/*
 * elim_stack.h
 *
 * File implements elimination stack, that allow to exchage
 * pop() and push() operations between two threads without
 * sleeping, and with some small delays.
 *
 */

#ifndef ELIM_STACK_H
#define ELIM_STACK_H

#include <iostream>
#include <string>
#include <chrono>
#include <atomic>

#include "rand.h"
#include "lf_stack.h"
#include "elim_array.h"

using namespace std;
template <typename T>
class eliminationStack: public lockFreeStack<T> {
protected:
    int _elim_array_capacity = 500;
    int _elim_timeout = 1000; // milliseconds
    eliminationArray<T> elim_array;

    bool correcting_working = true;
    int previous_elim_count, current_elim_count;
    int previous_pushes_count, current_pushes_count;
    thread *correcting_thread;

    bool try_push(node<T> * new_top)
    {
        hazard_pointer<node<T>> &hp_old_top = lockFreeStack<T>::hazard_pointers.acquire();
        node<T> * old_top = lockFreeStack<T>::top.load(), *temp = nullptr;

        do {
            temp = old_top;
            hp_old_top.remember(old_top);
            old_top = lockFreeStack<T>::top.load();
        } while(old_top != temp);
        new_top->next = old_top;
        bool result = lockFreeStack<T>::top.compare_exchange_strong(old_top, new_top);
        hp_old_top.release();
        return result;
    }

    node<T> * try_pop()
    {
        hazard_pointer<node<T>> &hp_old_top = lockFreeStack<T>::hazard_pointers.acquire();
        node<T> * old_top = lockFreeStack<T>::top.load(), *temp = nullptr;
        do {
            temp = old_top;
            hp_old_top.remember(old_top);
            old_top = lockFreeStack<T>::top.load();
        } while(old_top != temp);

        if (old_top == NULL) {
            hp_old_top.release();
            throw(STACK_IS_EMPTY);
        }
        node<T> * new_top;
        new_top = old_top->next;

        if (lockFreeStack<T>::top.compare_exchange_strong(old_top, new_top)) {
            hp_old_top.release();
            return old_top;
        } else {
            hp_old_top.release();
            throw(NOT_EXCHANGED);
        }
    }


    void correcting_job()
    {

        while(correcting_working) {
            this_thread::sleep_for(chrono::seconds(1));
            previous_elim_count = current_elim_count;
            current_elim_count = elim_array.get_eliminations_count();

            if (current_elim_count - previous_elim_count > 600) {
                // cout << "A lot of eliminations!" << endl;
            } else {
                // cout << "Not so eliminations" << endl;
            }

            previous_pushes_count = current_pushes_count;
            current_pushes_count = elim_array.get_pushes_count();
            // cout << current_pushes_count - previous_pushes_count<< endl;
        }

    }

public:

    eliminationStack():
        elim_array(_elim_array_capacity, _elim_timeout)
    {
        correcting_thread = new thread(&eliminationStack::correcting_job, this);
    }

    eliminationStack(int timeout, int capacity):
                _elim_array_capacity(capacity),
                _elim_timeout(timeout),
                elim_array(capacity, timeout)
    {
        correcting_thread = new thread(&eliminationStack::correcting_job, this);
    }

    ~eliminationStack()
    {
        correcting_working = false;
        correcting_thread->join();
    }

    void set_parametrs(int _timeout, int _capacity) {
        _elim_array_capacity = _capacity;
        _elim_timeout = _timeout;
        elim_array.set_parametrs(_elim_array_capacity, _elim_timeout);
    }

    // T pop() {
    //     while(true) {
    //         node<T> * returnNode;
    //         pair<T *, int> elim_result;
    //         T data;
    //
    //         try {
    //             returnNode = try_pop();
    //         }
    //         catch(int reason) {
    //             switch(reason) {
    //
    //                 case NOT_EXCHANGED:
    //                     elim_result = elim_array.visit(nullptr);
    //                     if (elim_result.second == VISIT_OK) {
    //                         if (elim_result.first != nullptr) {
    //                             //cout << "pop::exchanged" << "\n";
    //                             data = *elim_result.first;
    //                             return data;
    //                         }
    //                     }
    //                     continue;
    //                 break;
    //
    //                 case STACK_IS_EMPTY:
    //                     throw(STACK_IS_EMPTY);
    //                 break;
    //
    //                 default:
    //                     continue;
    //                 break;
    //
    //             }
    //         }
    //
    //         data = returnNode->data;
    //         delete returnNode;
    //         return data;
    //     }
    // }

    T pop() {
        node<T> *old_top = nullptr, *temp = nullptr;
        pair<T *, int> elim_result;
        T data;

        while(true) {
            hazard_pointer<node<T>> &hp = lockFreeStack<T>::hazard_pointers.acquire();

            do {
                temp = old_top;
                hp.remember(old_top);
                old_top = lockFreeStack<T>::top.load();
            } while(old_top != temp);

            if (old_top == nullptr) {
                hp.release();
                throw(STACK_IS_EMPTY);
            }

            if (lockFreeStack<T>::top.compare_exchange_strong(old_top, old_top->next)) {
                hp.release();
                data = old_top->data;
                delete old_top;
                return data;
            } else {
                hp.release();
                elim_result = elim_array.visit(nullptr);
                if (elim_result.second == VISIT_OK) {
                    if (elim_result.first != nullptr) {
                        //cout << "pop::exchanged" << "\n";
                        data = *elim_result.first;
                        return data;
                    }
                }
            }
        }
    }

    bool push(T data)
    {
        struct node<T> * new_top;
        new_top = new node<T>(data);
        node<T> *old_top = nullptr, *temp = nullptr;
        hazard_pointer<node<T>> &hp = lockFreeStack<T>::hazard_pointers.acquire();

        while(true) {

            old_top = lockFreeStack<T>::top.load();
            do {
                temp = old_top;
                hp.remember(old_top);
                old_top = lockFreeStack<T>::top.load();
            } while(old_top != temp);

            if (lockFreeStack<T>::top.compare_exchange_strong(old_top, new_top)) {
                hp.release();
                return true;
            } else {
                hp.release();
                pair<T *, int> elim_result = elim_array.visit(&data);
                if (elim_result.second == VISIT_OK) {
                    if (elim_result.first == nullptr) {
                        return true;
                    }
                }
            }
        }
        return true;
    }

    string get_statistics()
    {
        unsigned int eliminations = elim_array.get_eliminations_count();
        return "Elimination count: " + to_string(eliminations);
    }

};

#endif
