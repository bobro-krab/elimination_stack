/*
 * boost_stack.h
 *
 * This is wrapper to boost lock-free stack to test in main programm.
 *
 */

#ifndef BOOST_STACK_H
#define BOOST_STACK_H

#include <iostream>
#include <string>
#include <chrono>
#include <atomic>

#include <boost/lockfree/stack.hpp>
#include <boost/atomic.hpp>

#include "lf_stack.h"

using namespace std;
template <typename T>
class boostStack: public lockFreeStack<T> {
protected:
    boost::lockfree::stack<T> stack;

public:

    boostStack():
        stack(1000)
    {
    }

    ~boostStack()
    {
    }

    T pop() {
        T return_value;
        if (stack.pop(return_value)) {
            return return_value;
        } else {
            throw(STACK_IS_EMPTY);
        }
        return return_value;
    }

    bool push(T data)
    {
        return stack.push(data);
    }

    string get_statistics()
    {
        return "Boost no statistics";
    }

};

#endif
