#ifndef TIME_MEASURMENT
#define TIME_MEASURMENT

#include <sys/time.h>
#include <iostream>

double wtime()
{
    struct timeval t;
    gettimeofday(&t, NULL);
    return (double)t.tv_sec + (double)t.tv_usec * 1E-6;
}
#endif
