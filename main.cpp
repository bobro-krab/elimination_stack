#include <iostream>
#include <fstream>
#include <string>
#include <chrono>
#include <random>
#include <atomic>
#include <thread>
#include <future>

#include "lf_stack.h"
#include "elim_stack.h"
#include "boost_stack.h"

#include "rand.h"
#include "time.h"
#include "hazard.h"

using namespace std;

struct test_parametrs {
    int count;
    int thread_count;
};

struct stack_parametrs {
    int timeout;
    int capacity;
};

template <class T>
void thread_test(lockFreeStack<T>* s, T test_element, test_parametrs tp)
{
    for (int i = 0; i < tp.count; ++i){
        if (random_int(2)) {
            s->push(test_element);
        } else {
            try {
                s->pop();
            }
            catch (int i) {
            }
        }
    }
}

template <class T>
double test_stack(lockFreeStack<T>* s, T test_element, test_parametrs tp)
{
    double time;

    time = wtime();
    vector<thread *> threads;
    threads.resize(tp.thread_count);
    for(unsigned int i = 0; i < threads.size(); i++) {
        threads[i] = new thread(thread_test<T>, s, test_element, tp);
    }

    for(unsigned int i = 0; i < threads.size(); i++) threads[i]->join();
    time = wtime() - time;
    cout << time << " ";
    cout << s->get_statistics() << " ";
    s->clear();
    return time;
}

int main() {

    lockFreeStack<double> stack_l;

    // cout.sync_with_stdio(false);
    eliminationStack<double> stack_e;
    boostStack<double> stack_b;

    test_parametrs tp;
    tp.count = 1000000;
    tp.thread_count = 2;

    for (tp.thread_count = 2; tp.thread_count <= 16; tp.thread_count *= 2) {
        cout << tp.thread_count << " ";
        // test_stack(&stack_l, 6.34, tp);
        // cout << endl;
        // for (int timeout = 0; timeout < 1500; timeout += 100) {
        //     stack_e.set_parametrs(timeout, 10);
            // cout << timeout << " ";
            test_stack(&stack_e, 6.34, tp);
            test_stack(&stack_b, 6.34, tp);
            cout << endl;
        // }
    }

    return 0;
}
