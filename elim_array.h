#ifndef ELIM_ARRAY_H_BTVU2GYN
#define ELIM_ARRAY_H_BTVU2GYN

#include <atomic>
#include <chrono>
#include <iostream>

#include "rand.h"

// states of one exchanger cell
#define EMPTY 0
#define WAITING 1
#define BUSY 2

// error codes, thats describe return code
// timeout, when we dosen't wait for echaging element
#define VISIT_TIMEOUT 1
#define VISIT_OK 0

// exception parameters
#define ELIMINATION_TIMEOUT 2

using namespace std;
typedef int stamp;

template <class T>
//typedef int T;
class lockFreeExchanger {
    struct stamped_type {
        T * data;
        int stamp;
    };
    std::atomic<struct stamped_type> slot;

    // how many exchahges we done, while working
    unsigned int _exchanges_count;
    unsigned int _pushes_count;

public:

    lockFreeExchanger():
        _exchanges_count(0),
        _pushes_count(0)
    {
        slot.store({NULL, EMPTY}, std::memory_order_seq_cst);
    }

    /*
     * Try to exchange elements with exchanger,
     * throws exceptions when time is out.
     */
    T * exchange(T * myItem, int timeout)
    {
        stamped_type myItemAndStamp{myItem, WAITING};

        chrono::system_clock::time_point timeBound;
        timeBound = chrono::system_clock::now() + chrono::microseconds(timeout);

        while(true) {
            if (chrono::system_clock::now() > timeBound) {
                _pushes_count++;
                throw(ELIMINATION_TIMEOUT); // timeout exception
            }

            stamped_type yourItemAndStamp = slot.load();
            switch(yourItemAndStamp.stamp) {
                case EMPTY:
                    myItemAndStamp.stamp = WAITING;
                    if (slot.compare_exchange_strong(yourItemAndStamp,
                                        myItemAndStamp))
                    {

                        // we change old empty data with our,
                        // and it is now waiting 4 exchange
                        while(chrono::system_clock::now() < timeBound) {
                            yourItemAndStamp = slot.load();
                            if (yourItemAndStamp.stamp == BUSY) {
                                slot.store(stamped_type{NULL, EMPTY});
                                _exchanges_count++;
                                return yourItemAndStamp.data;
                            }
                        }

                        if (slot.compare_exchange_strong(myItemAndStamp,
                                        stamped_type{NULL, EMPTY}))
                        {
                            // nobody change our data, and we free slot,
                            // and sending time exception
                            _pushes_count++;
                            throw(ELIMINATION_TIMEOUT);
                        } else {
                            // somebody change data, CES put it in myitemandstamp
                            // we need to return it, and free slot
                            slot.store(stamped_type{ NULL, EMPTY });
                            return myItemAndStamp.data;
                        }
                    }

                break; // EMPTY

                case WAITING:
                    myItemAndStamp.stamp = BUSY;
                    if (slot.compare_exchange_strong(yourItemAndStamp,
                                        myItemAndStamp)) {
                        return yourItemAndStamp.data;
                    }

                break; // WAITING

                case BUSY:
                break; // BUSY
                default:
                break;
            }
        }
    }

    unsigned int get_exchanges_count()
    {
        unsigned int ex = _exchanges_count;
        _exchanges_count = 0;
        return ex;
    }

    unsigned int get_pushes_count()
    {
        return _pushes_count;
    }

};

template <typename T>
class eliminationArray {
    int duration; // in milliseconds
    unsigned int range;
    vector<lockFreeExchanger<T> *> _exchangers;

public:

    eliminationArray(int capacity, int timeout_duration)
    {
        duration = timeout_duration;
        _exchangers.resize(capacity);
        range = 1;
        for (unsigned int i = 0; i < _exchangers.size(); ++i){
            _exchangers[i] = new lockFreeExchanger<T>();
        }
    }

    ~eliminationArray()
    {
        for (unsigned int i = 0; i < _exchangers.size(); ++i){
            delete _exchangers[i];
        }
    }

    void set_parametrs(size_t new_capacity, int timeout)
    {
        duration = timeout;
        if (new_capacity != _exchangers.size()) {
            for (unsigned int i = 0; i < _exchangers.size(); ++i){
                delete _exchangers[i];
            }

            _exchangers.resize(new_capacity);

            for (unsigned int i = 0; i < _exchangers.size(); ++i){
                _exchangers[i] = new lockFreeExchanger<T>();
            }
        }
        range = 1;
    }

    /*
     * Returns summ of current exchanges count.
     */
    unsigned int get_eliminations_count()
    {
        unsigned int _exchanges_count = 0;
        for (unsigned int i = 0; i < _exchangers.size(); ++i){
            _exchanges_count += _exchangers[i]->get_exchanges_count();
        }
        return _exchanges_count;
    }

    unsigned int get_pushes_count()
    {
        unsigned int _pushes_count = 0;
        for (unsigned int i = 0; i < _exchangers.size(); ++i){
            _pushes_count += _exchangers[i]->get_pushes_count();
        }
        return _pushes_count;
    }

    /*
     */
    pair<T *, int> visit(T * value)
    {
        int slot = random_int(_exchangers.size());
        // int slot = random_int(range);
        T * temp;
        try {
            temp = _exchangers[slot]->exchange(value, duration);
        }
        catch (int i ) {
            // range = range / 2 + 1;
            return pair<T *, int>(NULL, VISIT_TIMEOUT);
        }

        // range = (range * 2 > _exchangers.size())?_exchangers.size():range * 2;
        return pair<T *, int> (temp, VISIT_OK);
    }


};

#endif /* end of include guard: ELIM_ARRAY_H_BTVU2GYN */
